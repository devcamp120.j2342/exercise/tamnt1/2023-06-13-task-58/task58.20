package com.devcamp.drink.respository;

import org.springframework.data.repository.CrudRepository;

import com.devcamp.drink.models.Drink;

public interface PizzaRepository extends CrudRepository<Drink, Integer> {

}
