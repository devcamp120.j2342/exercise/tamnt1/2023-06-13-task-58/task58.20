package com.devcamp.drink.services;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Service;

import com.devcamp.drink.models.Drink;
import com.devcamp.drink.respository.PizzaRepository;

@Service
public class DrinkService {
    private final PizzaRepository pizzaRepository;

    public DrinkService(PizzaRepository pizzaRepository) {
        this.pizzaRepository = pizzaRepository;
    }

    public void createDrinks() {
        Date currentDate = new Date();

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Drink drink1 = new Drink("TRATAC", "Trà tắc", 10000, null, dateFormat.format(currentDate),
                dateFormat.format(currentDate));
        Drink drink2 = new Drink("COCA", "Cocacola", 15000, null, dateFormat.format(currentDate),
                dateFormat.format(currentDate));
        Drink drink3 = new Drink("PEPSI", "Pepsi", 17000, null, dateFormat.format(currentDate),
                dateFormat.format(currentDate));

        pizzaRepository.save(drink1);
        pizzaRepository.save(drink2);
        pizzaRepository.save(drink3);
    }

    public List<Drink> getDrinks() {
        List<Drink> drinks = (List<Drink>) pizzaRepository.findAll();
        return drinks;
    }
}
