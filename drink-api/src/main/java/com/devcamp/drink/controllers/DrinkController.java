package com.devcamp.drink.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.drink.models.Drink;
import com.devcamp.drink.services.DrinkService;

@RestController
@CrossOrigin
public class DrinkController {
    @Autowired
    private DrinkService drinkService;

    @GetMapping("/drinks")
    public List<Drink> getDrinkList() {
        return drinkService.getDrinks();
    }

    @GetMapping("/create-drinks")
    public void creatDrink() {
        drinkService.createDrinks();
    }

}
